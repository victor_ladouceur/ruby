FROM gfunk/centos7:latest

# install ruby, required tools and clean cache
RUN yum -y install ruby gcc rubygems ruby-devel make && yum clean all
