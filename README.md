# Open source Ruby docker image

This Ruby docker image is based on custom [Centos 7 image] (https://hub.docker.com/r/gfunk/centos7/)

---

## Features

- ruby
- rubygems
- gcc
- rubydevel
- make
